from peewee import *
import datetime


db = SqliteDatabase('corpus.db')

class TextData(Model):
    identifier = CharField(primary_key=True)
    artist = CharField()
    artwork = CharField()
    type = IntegerField()
    content = CharField()

    class Meta:
        database = db
        table_name = 'text_data'